package example

import org.scalatest._

class TreeCounterSpec extends FlatSpec with Matchers {
  "The TreeCounter object" should "return accurate values" in {
    val resList = List(1, 1, 2, 5, 14, 42, 132, 429, 1430, 4862)
    var i = 0
    for (i <- 0 until 10){
      TreeCounter.catalanNumber(i) shouldEqual resList(i)
    }

  }
}
