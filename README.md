# FX CLUB Data Engineer Code Challenge

## Scala solution

I wrote the solution in Scala using sbt to build, test and run the code.
Used Giter8 to generate the default Scala project structure with:

	sbt new scala/scala-seed.g8

## Instructions
Make sure you have Java, Scala and sbt installed.

Clone the repository

	git clone git@bitbucket.org:dogmeat/fx-challenge-scala.git

Go to project directory

	 cd fx-challenge-scala/treecounter/

Run sbt in console

	sbt

The project should load with this.
Run test with:

	> test

I made one test, but it tests whether the numbers from 0-9 return expected results.

	[info] Compiling 1 Scala source to /Users/dgmt/IdeaProjects/fx-challenge-scala/treecounter/target/scala-2.12/test-classes...
	[info] TreeCounterSpec:
	[info] The TreeCounter object
	[info] - should return accurate values
	[info] Run completed in 195 milliseconds.
	[info] Total number of tests run: 1
	[info] Suites: completed 1, aborted 0
	[info] Tests: succeeded 1, failed 0, canceled 0, ignored 0, pending 0
	[info] All tests passed.
	[success] Total time: 1 s, completed Nov 9, 2017 11:23:36 AM
 
After the tests pass, run it with a commandline parameter of the number of nodes for which you want to do a calculation.

	> run 3

The sbt console will respond similar to this:

	[info] Compiling 1 Scala source to /Users/dgmt/IdeaProjects/fx-challenge-scala/treecounter/target/scala-2.12/classes...
	[info] Running example.TreeCounter 3
	Number of binary search trees for 3 nodes is 5
	[success] Total time: 4 s, completed Nov 9, 2017 11:06:23 AM

