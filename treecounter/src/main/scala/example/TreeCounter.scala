package example

object TreeCounter{
  def main(args: Array[String]){
    if (args.length == 0) println("No args!")
    else {
      var n:Int = args.last.toInt
      var res:BigInt = catalanNumber(n)
      println(f"Number of binary search trees for $n nodes is $res")
    }
  }

  def catalanNumber(n: Int): BigInt = {
    n match {
      case 0 | 1 => 1
      case _ =>  (0 until n).map(i => catalanNumber(i) * catalanNumber(n - i - 1)).sum
    }
  }
}
